/**
Godot Math. Reimplementation of Godot Math library but in D, with the same interface.

Copyright: Guillaume Piolat 2020.
License:   $(LINK2 http://www.boost.org/LICENSE_1_0.txt, Boost License 1.0)
*/
module gm.vector2;

import std.math: abs, ceil, floor;

nothrow @nogc @safe:

/// Vector used for 2D math.
struct Vector2
{
nothrow @nogc @safe:

    /// The vector's X component. Also accessible by using the index position [0].
    union
    {
        float x = 0;
        float width;
    }

    /// The vector's Y component. Also accessible by using the index position [1].
    union
    {
        float y = 0;
        float height;
    }

    /// Constructs a new Vector2 from the given `x` and `y`.
    this( float x, float y )
    {
        this.x = x;
        this.y = y;
    }

    /// Returns a new vector with all components in absolute values (i.e. positive).
    Vector2 abs()
    {
        return Vector2(.abs(x), .abs(y));
    }
    unittest
    {
        Vector2 v = Vector2(-4, -5);
        assert(v.abs() == Vector2(4, 5));
    }


    /// float angle ( )
    /// float angle_to ( Vector2 to )
    /// float angle_to_point ( Vector2 to )

    /// Returns the aspect ratio of this vector, the ratio of `x` to `y`.
    float aspect()
    {
        return x / y;
    }
    unittest
    {
        Vector2 v = Vector2(-4, -4);
        assert(v.aspect() == 1);
    }

    /// Returns the vector "bounced off" from a plane defined by the given normal.
    Vector2 bounce(Vector2 n)
    {
        return -reflect(n);
    }

    /// Returns the vector reflected from a plane defined by the given normal.
    Vector2 reflect(Vector2 n)
    {
        return 2.0 * n * this.dot(n) - *this;
    }

}