/**
Godot Math. Reimplementation of Godot Math library but in D, with the same interface.

Copyright: Guillaume Piolat 2020.
License:   $(LINK2 http://www.boost.org/LICENSE_1_0.txt, Boost License 1.0)
*/
module gm;

public import gm.vector2;